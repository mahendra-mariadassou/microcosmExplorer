
<!-- README.md is generated from README.Rmd. Please edit that file -->

# microcosmExplorer

<!-- badges: start -->

[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
<!-- badges: end -->

The goal of microcosmExplorer is to explore the microcosm data.

## Installation

You can install microcosmExplorer from the MIA gitlab with

``` r
remotes::install_gitlab(
  host = "forgemia.inra.fr", 
  repo = "mahendra-mariadassou/microcosmExplorer"
)
```

The package has a single function to launch a shiny app:

``` r
microcosmExplorer::run_app()
```
