# microcosmExplorer 0.1.0.1

* Update metadata in phyloseq objects: add covariates `MC_entre1Met3M`, `MC_entre3Met7M` and `M_entre1Met3M`

# microcosmExplorer 0.1.0.0

* Update metadata in phyloseq objects
* Added all time points and functional modules for data exploration
* Added a `NEWS.md` file to track changes to the package.
